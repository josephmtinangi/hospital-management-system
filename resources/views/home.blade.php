@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">
                        <ul>
                            <li>system can register a new User</li>
                            <li>system can make appointment for that patient</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
