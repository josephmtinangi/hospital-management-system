@if (Auth::user()->hasRole('admin'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item active">Dashboard</a>
        <a href="/users" class="list-group-item">Manage users</a>
        <a href="/doctors" class="list-group-item">Manage doctors</a>
    </div>
    <div class="list-group">
        <a href="#" class="list-group-item active">Settings</a>
        <a href="/payment-types" class="list-group-item">Payment Types</a>
        <a href="/departments" class="list-group-item">Departments</a>
        <a href="/laboratory-tests" class="list-group-item">Laboratory Tests</a>
        <a href="#" class="list-group-item">Backup</a>
    </div>
@endif

@if (Auth::user()->hasRole('receptionist'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item active">Dashboard</a>
        <a href="/patients" class="list-group-item">Patients</a>
        <a href="/payments" class="list-group-item">Payments</a>
    </div>
@endif
@if (Auth::user()->hasRole('doctor'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item active">Dashboard</a>
        <a href="/patients" class="list-group-item">Appointments</a>
    </div>
@endif

@if (Auth::user()->hasRole('pharmacist'))
    <!-- nav links here -->
    <div class="list-group">
        <a href="/home" class="list-group-item active">Dashboard</a>
        <a href="/medicines" class="list-group-item">Manage Medicines</a>
        <a href="/pharmace/prescriptions" class="list-group-item">prescriptions</a>
    </div>
@endif

@if (Auth::user()->hasRole('laboratory-attendant'))
    <div class="list-group">
        <a href="/" class="list-group-item"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
        <a href="/lab-tests" class="list-group-item"><i class="fa fa-stethoscope fa-fw"></i> Laboratory Tests</a>
    </div>
@endif