<div class="form-group">
    <label for="name" class="col-sm-2 control-label">Test name</label>
    <div class="col-sm-10">
        <input type="text" name="name" id="name" value="{{ $laboratoryTest->name or old('name') }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="amount" class="col-sm-2 control-label">Price</label>
    <div class="col-sm-10">
        <input type="text" name="price" id="price" value="{{ $laboratoryTest->price or old('price') }}"
               class="form-control">
    </div>
</div>


