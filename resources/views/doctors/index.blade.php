@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>All doctors</strong>
                    </div>

                    <div class="panel-body">
                        @if($doctors->count())
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="text-right">#</th>
                                    <th>Full name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Phone number</th>
                                    <th>Department</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($doctors as $doctor)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>
                                            <a href="/doctors/{{ $doctor->id }}">
                                                Dr. {{ $doctor->first_name }} {{ $doctor->middle_name }} {{ $doctor->last_name }}
                                            </a>
                                        </td>
                                        <td>{{ $doctor->username }}</td>
                                        <td>{{ $doctor->email }}</td>
                                        <td>{{ $doctor->phone }}</td>
                                        <td>
                                            @if($doctor->roles()->count())
                                                @foreach($doctor->roles as $role)
                                                    <span class="badge">{{ $role->name }}</span>
                                                @endforeach
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Doctor found
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
