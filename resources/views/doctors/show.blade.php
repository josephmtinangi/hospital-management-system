@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            {{ $doctor->name }}
                        </h3>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered table-hover">
                            <tbody>
                                <tr>
                            <th>Full Name</th>
                                <td>     
    {{ $doctor->first_name }}  {{ $doctor->middle_name }}  {{ $doctor->last_name }}  
                                </td>
                                </tr>
                                
                                <tr>
                                <th>Username</th>
                                <td>
                                  {{ $doctor->username }}   
                                </td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>{{ $doctor->email }}</td>
                                </tr>
                                 <tr>
                                <th>Department</th>
                                <td>
                                    @if($doctor->departments->count())
                                        <ul>
                                            @foreach($doctor->departments as $department)
                                                <li>{{ $department->name }}</li>
                                            @endforeach
                                        </ul>
                                    @else
                                     -
                                    @endif                   
                                </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">doctor roles</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-10">
<form action="/doctors/{{ $doctor->id }}/departments" method="POST" role="form">
                                    {{ csrf_field() }}
<div class="form-group">
    <label for="for departmet" class="col-md-2 col-md-offset-3 control-label">Change department</label>
   <div class="col-md-3 col-md-offset-0">
       <select class="form-control" name="department" id="department">
           <option value="">select department</option>
           @foreach($departments as $department)
           <option value="{{ $department->id }}">{{ $department->name }}</option>
           @endforeach
       </select>
   </div>
</div>
  <div class="form-group">
      <button type="submit" class="btn btn-primary">Save</button>
  </div>                                                            
                                                               
 </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
