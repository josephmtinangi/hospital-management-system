@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Payments</div>

                    <div class="panel-body">
                        @if($payments->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Patient</th>
                                    <th>Type of Payment</th>
                                    <th class="text-right">Amount (Tshs)</th>
                                    <th>Date of Payment</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1
                                @endphp
                                @foreach($payments as $payment)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            <a href="/patients/{{ $payment->patient_id }}">
                                                {{ $payment->patient->first_name  }}
                                                {{ $payment->patient->middle_name  }}
                                                {{ $payment->patient->last_name  }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $payment->paymentType->display_name }}
                                        </td>
                                        <td class="text-right">
                                            {{ $payment->paymentType->amount }}
                                        </td>
                                        <td>{{ $payment->created_at }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No Payment
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
