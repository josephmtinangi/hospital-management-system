@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Laboratory Tests</div>

                    <div class="panel-body">
                        @if($lab_tests->count())
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Patient</th>
                                    <th>Test</th>
                                    <th>Result</th>
                                    <th>Action</th>
                                    <th>Conducted</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1 @endphp
                                @foreach($lab_tests as $lab_test)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>
                                            {{ $lab_test->patient->first_name }}
                                            {{ $lab_test->patient->middle_name }}
                                            {{ $lab_test->patient->last_name }}
                                        </td>
                                        <td>
                                            {{ $lab_test->labTest->name }}
                                        </td>
                                        <td>
                                            @if($lab_test->result)
                                                {{ $lab_test->result }}
                                            @else
                                                Not Conducted
                                            @endif
                                        </td>
                                        <td>
                                            @if($lab_test->is_conducted == false)
                                                <a href="/lab-tests/{{ $lab_test->id }}/conduct"
                                                   class="btn btn-primary btn-sm">
                                                    Conduct
                                                </a>
                                            @else
                                                <span class="label label-success">Conducted</span>
                                            @endif
                                        </td>
                                        <td>{{ $lab_test->updated_at->diffForHumans() }}</td>
                                        <td>{{ $lab_test->created_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info">
                                No Laboratory tests to conduct
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
