@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">All Appointments</h3>
                    </div>

                    <div class="panel-body">
                        @if($appointments->count())
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Patient</th>
                                    <th>Doctor</th>
                                    <th>Created</th>
                                    <th>Updated</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($appointments as $appointment)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>
                                            <a href="/patients/{{ $appointment->patient->id }}">
                                                {{ $appointment->patient->first_name }}
                                                {{ $appointment->patient->middle_name }}
                                                {{ $appointment->patient->last_name }}
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/doctors/{{ $appointment->doctor->id }}">
                                                {{ $appointment->doctor->first_name }}
                                                {{ $appointment->doctor->middle_name }}
                                                {{ $appointment->doctor->last_name }}
                                            </a>
                                        </td>
                                        <td>
                                            {{ $appointment->created_at }}
                                        </td>
                                        <td>
                                            {{ $appointment->updated_at }}
                                        </td>
                                        <td>
                                            <a href="#">Edit</a>
                                        </td>
                                        <td>
                                            <a href="#">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Appointment
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
