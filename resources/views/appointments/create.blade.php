@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Make appointment
                        for {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}</div>

                    <div class="panel-body">
                        <form action="/patients/{{ $patient->id }}/appointments" method="POST" role="form">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="doctor_id">
                                    Select a doctor for consultation
                                </label>
                                <select name="doctor_id" id="doctor_id" class="form-control">
                                    <option value="">-Select-</option>
                                    @foreach($doctors as $doctor)
                                        <option value="{{ $doctor->id }}">
                                            {{ $doctor->first_name }}
                                            {{ $doctor->middle_name }}
                                            {{ $doctor->last_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="doctor_id">
                                    Select the consultation fee
                                </label>
                                <select name="payment_type_id" id="payment_type_id" class="form-control">
                                    <option value="">-Select-</option>
                                    @foreach($paymentTypes as $paymentType)
                                        <option value="{{ $paymentType->id }}">
                                            {{ $paymentType->display_name }} -
                                            {{ $paymentType->amount }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="description">Notes (Option)</label>
                                <textarea name="description" id="description" rows="5" class="form-control"></textarea>
                            </div>


                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
