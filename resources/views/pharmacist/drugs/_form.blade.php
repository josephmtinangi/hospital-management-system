<div class="form-group">
    <label for="name" class="col-sm-2 col-sm-offset-3 control-label">Name</label>
    <div class="col-sm-6">
        <input type="text" name="name" id="name" value="{{ $drug->name or old('name') }}" class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="item" class="col-sm-2 col-sm-offset-3 control-label">No of items</label>
    <div class="col-sm-6">
        <input type="text" name="item" id="item" value="{{ $drug->quantity or old('item') }}" class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="price" class="col-sm-2 col-sm-offset-3 control-label">Price per item</label>
    <div class="col-sm-6">
        <input type="text" name="price" id="price" value="{{ $drug->price or old('price') }}" class="form-control">
    </div>
</div>
