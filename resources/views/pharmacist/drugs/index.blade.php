@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="medicines/create" class="btn btn-primary">Add New</a>
                        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>or Upload</a>
                        <div class="modal fade" id="modal-id">
                            <div class="modal-dialog">
                                <form method="POST" action="/medicines/upload" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                    aria-hidden="true">&times;</button>
                                            <h4 class="modal-title">Modal title</h4>
                                        </div>
                                        <div class="modal-body">

                                            <input type="file" name="user_file">

                                            <br>

                                            <a href="/download/Medicines.xlsx">Download Sample</a>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel
                                            </button>
                                            <button type="submit" class="btn btn-primary">Upload</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        @if($drugs->count())
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th class="text-right">#</th>
                                    <th>Name</th>
                                    <th>Items</th>
                                    <th>price</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($drugs as $drug)
                                    <tr>
                                        <td class="text-right">{{ $i++ }}.</td>
                                        <td>

                                            {{ $drug->name }}

                                        </td>
                                        <td>{{ $drug->quantity }}</td>
                                        <td>{{ $drug->price }}</td>
                                        <td>
                                            <a href="/medicines/{{ $drug->id }}/edit"
                                               class="btn btn-info btn-sm">Edit</a>
                                        </td>
                                        <td>
                                            <a class="btn btn-danger btn-sm" data-toggle="modal" href='#delete-drug'>Delete</a>
                                            <div class="modal fade" id="delete-drug">
                                                <div class="modal-dialog">
                                                    <form method="POST" action="/medicines/{{ $drug->id }}">
                                                        {{ csrf_field() }}
                                                        {{ method_field('DELETE') }}


                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;</button>
                                                                <h4 class="modal-title">Delete {{ $drug->name }}</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                Delete permanently {{ $drug->name }}?
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default"
                                                                        data-dismiss="modal">Cancel
                                                                </button>
                                                                <button type="submit" class="btn btn-danger">Delete
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Medicine available
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
