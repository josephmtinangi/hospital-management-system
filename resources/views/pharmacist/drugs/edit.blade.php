@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Edit {{ $drug->name }}</h3>
                    </div>

                    <div class="panel-body">
                        @include('errors.list')

                        <form style="width:800px;" action="/medicines/{{ $drug->id }}" method="POST"
                              class="form-horizontal" role="form">
                            {{ csrf_field() }}
                            {{ method_field('PATCH') }}

                            @include('pharmacist.drugs._form')


                            <div class="form-group">
                                <div class="col-sm-6 col-sm-offset-5">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
