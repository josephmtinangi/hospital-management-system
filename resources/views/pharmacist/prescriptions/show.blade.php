@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <strong>All patients from doctor</strong>
                    </div>

                    <div class="panel-body">
                        @if($patients->count())
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                     <th>pat No</th>
                                    <th>Patient name</th>
                                    <th>doctor Id</th>
                                    <th>Create_at</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach($patients as $patient)
                                    <tr>
                                        <td>{{ $i++ }}.</td>
                                        <td>{{ $patient->patient_id }}.</td>
                                        <td>
                                         <a href="/users/{{ $patient->id }}">
                                                {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}
                                            </a>
                                        </td>
                                        <td>{{ $patient->doctor_id }}.</td>
                                         <td>{{ $patient->created_at }}.</td>
                                     </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                No Medicine available
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
