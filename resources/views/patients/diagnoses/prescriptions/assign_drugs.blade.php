@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                @include('errors.list');
                                <h3 class="panel-title">Assign drugs</h3>
                            </div>

                            <div class="panel-body">
                                @include('patients.diagnoses.prescriptions._form_assign_drug')
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))

                            @include('partials.patient.rightsidebar')
                        @else
                            @include('partials/patient/sidebar')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

   

   
   
   
   