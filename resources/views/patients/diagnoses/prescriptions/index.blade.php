@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-sm-12">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Patient prescriptions</h3>
                            </div>
                            <div class="panel-body">
                                @if($patient->diagnoses->count())
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Disease</th>
                                            <th>Diagnoses id</th>
                                            <th>Created</th>
                                            <th>doctor id</th>
                                            <th>Full details</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1
                                        @endphp
                                        @foreach($prescriptions as $prescription)
                                            <tr>
                                                <td>{{ $i++ }}.</td>
                                                <td>{{ $prescription->disease }}</td>
                                                <td>{{ $prescription->diagnosis_id }}</td>
                                                <td>{{ $prescription->created_at }}</td>
                                                <td>{{ $prescription->doctor_id }}</td>
                                                <td>
                                                    <a href="/patients/{{ $patient->id }}/diagnoses/{{ $diagnosis->id }}">
                                                        Click to view
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-info">
                                        This patient has not undergone any diagnosis yer.
                                    </div>
                                @endif
                            </div>
                            <div class="panel-footer">
                                <a href="/patients/{{ $patient->id}}/diagnoses/{{ $diagnosis->id}}/prescriptions/create" class="btn btn-success">Add prescriptions</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                       @if (Auth::user()->hasRole('doctor'))

                       @include('partials.patient.rightsidebar')
                       @else
                       @include('partials/patient/sidebar')
                      @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
