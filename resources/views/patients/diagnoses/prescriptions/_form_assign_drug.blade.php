<form action="/patients/{{$patient->id}}/diagnoses/{{$diagnosesId}}/prescriptions/{{Request::segment(6)}}/assigndrugs"
      method="post">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="drug">Select drug:</label>
        <select id="drug" class="form-control" name="drug">
            <option value="">select drug to assign</option>
            @foreach($drugs as $drug)
                <option value="{{$drug->id}}">{{ $drug->name }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="dose">Enter dose</label>
        <input type="text" name="dose" id="dose" class="form-control"
               placeholder="dose">
    </div>
    <div class="form-group">
        <label for="dose">Description(optional)</label>
        <textarea type="text" name="description" id="dose"
                  class="form-control"></textarea>
    </div>
    <div class="form-group">
        <a href="/patients/{{$patient->id}}/diagnoses/{{$diagnosesId}}/prescriptions/create"
           class="btn btn-primary">Finish</a>
        <button type="submit" class="btn btn-primary">Add</button>
    </div>
</form>