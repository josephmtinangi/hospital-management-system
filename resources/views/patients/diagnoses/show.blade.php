@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Patient Diagnoses</h3>
                            </div>

                            <div class="panel-body">
                                <h2>Summary</h2>

                                {{ $diagnosis->summary }}

                                @if($diagnosis->body)
                                    <h2>Full Diagnosis</h2>
                                    {{ $diagnosis->body }}
                                @endif

                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Laboratory Tests</h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    @if($diagnosis->patient_lab_tests->count())
                                        <table class="table table-bordered table-hover">
                                            <thead>
                                            <tr>
                                                <th>SN</th>
                                                <th>TEST NAME</th>
                                                <th>RESULT</th>
                                                <th>Created</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php $i = 1 @endphp
                                            @foreach($diagnosis->patient_lab_tests as $patient_lab_test)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    <td>{{ $patient_lab_test->labTest->name }}</td>
                                                    <td>{{ $patient_lab_test->result }}</td>
                                                    <td>{{ $patient_lab_test->created_at->diffForHumans() }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-info">No Lab Tests</div>
                                    @endif
                                </div>
                                <a href="/patients/{{ $patient->id}}/diagnoses/" class="btn btn-primary">go back</a>
                                <a href="/patients/{{ $patient->id}}/diagnoses/{{ $diagnosis->id}}/prescriptions/create"
                                   class="btn btn-primary">Prescribe</a>
                            </div>
                        </div>


                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))

                            @include('partials.patient.rightsidebar')
                        @else
                            @include('partials/patient/sidebar')
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
    </div>
@endsection
