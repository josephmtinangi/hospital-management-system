@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
          
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">New Diagnosis</h3>
                            </div>

                            <div class="panel-body">
                           
                                <form action="/patients/{{ $patient->id }}/diagnoses" method="POST" role="form">
                                    {{ csrf_field() }}

                                    @include('patients.diagnoses._form')
                                      <a href="#" class="btn btn-primary">Back</a> 
                                    <button type="submit" class="btn btn-primary">Next</button>
                                </form>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))

                       @include('partials.patient.rightsidebar')
                       @else
                       @include('partials/patient/sidebar')
                      @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
