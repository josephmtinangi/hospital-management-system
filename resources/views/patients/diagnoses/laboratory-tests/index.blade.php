@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="jumbotron">
                            <h2>
                                {{ $patient->first_name }}
                                {{ $patient->middle_name }}
                                {{ $patient->last_name }}
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">New Laboratory Test</h3>
                            </div>

                            <div class="panel-body">
                                @if($diagnosis->labTests->count())
                                   @include('patients.diagnoses.laboratory-tests.table')
                                @else
                                    <div class="alert alert-info">
                                        No any Laboratory test have been conducted for this diagnosis yet.
                                    </div>
                                @endif
                            </div>
                            <div class="panel-footer">
                                <a href="/patients/{{ $patient->id }}/diagnoses/{{ $diagnosis->id }}/laboratory-tests/create"
                                   class="btn btn-info">New Laboratory Test</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @include('partials/patient/sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
