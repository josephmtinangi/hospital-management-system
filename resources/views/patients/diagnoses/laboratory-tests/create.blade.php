@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">

                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Laboratory tests(Optional)</h3>
                            </div>

                            <div class="panel-body">
                                @include('errors.list')
                                <ul>
                                    @foreach($patient->patient_lab_tests as $lab_test)
                                        <li>{{ $lab_test->labTest->name }} - Tshs {{ $lab_test->labTest->price }}</li>
                                    @endforeach
                                </ul>
                                <form action="/patients/{{$patient->id}}/diagnoses/{{Request::segment(4)}}/laboratory-tests/store"
                                      method="POST" role="form">
                                    {{ csrf_field() }}

                                    @include('patients.diagnoses.laboratory-tests._form')
                                    <button type="submit" name="submit" id="submit" class="btn btn-primary btn-sm"> Add
                                    </button>
                                </form>
                            </div>
                            <div class="panel-footer">
                                <a href="/patients/{{$patient->id}}/diagnoses{{$diagnosis->id}}"
                                   class="btn btn-primary btn-sm">Finish</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @if (Auth::user()->hasRole('doctor'))

                            @include('partials.patient.rightsidebar')
                        @else
                            @include('partials/patient/sidebar')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
