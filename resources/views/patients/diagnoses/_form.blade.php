<div class="form-group">
    <label for="summary">Summary</label>
    <input type="text" name="summary" id="summary" rows="2" class="form-control">
</div>
<div class="form-group">
    <label for="body">Full Description</label>
    <textarea name="body" id="body" rows="10" class="form-control"></textarea>
</div>
