<div class="form-group">
    <label for="first_name" class="col-sm-2 control-label">First Name</label>
    <div class="col-sm-10">
        <input type="text" name="first_name" id="first_name" value="{{ $user->first_name or old('first_name') }}"
               class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="middle_name" class="col-sm-2 control-label">Middle Name (Option)</label>
    <div class="col-sm-10">
        <input type="text" name="middle_name" id="middle_name" value="{{ $user->middle_name or old('middle_name') }}"
               class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="last_name" class="col-sm-2 control-label">Last Name</label>
    <div class="col-sm-10">
        <input type="text" name="last_name" id="last_name" value="{{ $user->last_name or old('last_name')}}"
               class="form-control">
    </div>
</div>
<div class="form-group">
    <label for="gender" class="col-sm-2 control-label">Gender</label>
    <div class="col-sm-10">
        <div class="radio">
            <label>
                <input type="radio" name="gender" id="gender"
                       value="Female" {{ isset($user) ? $user->gender == 'Female' ? 'checked' : '' : '' }}>
                Female
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="gender" id="gender"
                       value="Male" {{ isset($user) ? $user->gender == 'Male' ? 'checked' : '' : '' }}>
                Male
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="dob" class="col-sm-2 control-label">Date of Birth</label>
    <div class="col-sm-10">
        <input type="date" name="dob" id="dob" value="{{ isset($user->dob) ? $user->dob->format('Y-m-d') : '' }}"
               class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="phone" class="col-sm-2 control-label">Phone</label>
    <div class="col-sm-10">
        <input type="tel" name="phone" id="phone" value="{{ $user->phone or old('phone')}}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
        <input type="email" name="email" id="email" value="{{ $user->email or old('email')}}" class="form-control">
    </div>
</div>

<div class="form-group">
    <label for="address" class="col-sm-2 control-label">Address</label>
    <div class="col-sm-10">
        <input type="text" name="address" id="address" value="{{ $user->address or old('address') }}"
               class="form-control">
    </div>
</div>
