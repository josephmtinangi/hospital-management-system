@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="well">
                            <h2>
                                {{ $patient->first_name }}
                                {{ $patient->middle_name }}
                                {{ $patient->last_name }}
                                (Born {{ $patient->dob->diffForHumans() }})
                            </h2>
                            <p>
                                <i class="fa fa-phone"></i> {{ $patient->phone }}
                                <i class="fa fa-envelope"></i> {{ $patient->email }}
                                <i class="fa fa-map-marker"></i> {{ $patient->address }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Patient Appointments</h3>
                            </div>

                            <div class="panel-body">
                                @if($patient->appointments->count())
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>Doctor</th>
                                            <th>Status</th>
                                            @if(Auth::user()->hasRole('doctor'))
                                                <th>Full Details</th>
                                            @endif
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1
                                        @endphp
                                        @foreach($patient->appointments as $appointment)
                                            <tr>
                                                <td class="text-right">{{ $i++ }}.</td>
                                                <td>{{ $appointment->doctor->username }}</td>
                                                <td>Paid</td>
                                                @if(Auth::user()->hasRole('doctor'))
                                                    <td>
                                                        <a href="/patients/{{ $patient->id }}/appointments/{{ $appointment->id }}">Click
                                                            to
                                                            View</a>
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-info">
                                        This patient has not undergone any diagnosis yet.
                                    </div>
                                @endif
                            </div>
                            <div class="panel-footer">
                                <a href="/patients/{{ $patient->id }}/appointments/create" class="btn btn-success">New
                                    Appointment</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @include('partials/patient/sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
