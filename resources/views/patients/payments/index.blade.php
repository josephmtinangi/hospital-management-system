@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="jumbotron">
                            <h2>
                                {{ $patient->first_name }}
                                {{ $patient->middle_name }}
                                {{ $patient->last_name }}
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-10">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Patient payments</h3>
                            </div>

                            <div class="panel-body">
                                @if($patient->payments->count())
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>PayId</th>
                                            <th>Payment type</th>
                                            <th>Amount</th>
                                              <th>Status</th>
                                            <th>Date for payments</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1
                                        @endphp
                                        @foreach($patient->payments as $payment)
                                            <tr>
                                                <td class="text-right">{{ $i++ }}.</td>
                                                <td>{{ $payment->id }}</td>
                                                <td>{{ $payment->payment_type_id }}</td>
                                                <td>{{ $payment->PaymentType->payment_type_id }}</td>
                                                <th></th>
                                                <td>
                                                    <a href="#">Click
                                                        to
                                                        View</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="alert alert-info">
                                        This patient has not make any payments yet.
                                    </div>
                                @endif
                            </div>
                            <div class="panel-footer">
<a href="/patients/{{ $patient->id }}/payments/create" class="btn btn-success">New
                                    Payments</a>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-2">
                        @include('partials/patient/sidebar')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
