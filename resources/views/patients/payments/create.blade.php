@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">Make payments
                        for {{ $patient->first_name }} {{ $patient->middle_name }} {{ $patient->last_name }}</div>

                    <div class="panel-body">
                        <form action="/patients/{{ $patient->id }}/payments" method="POST" role="form">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="doctor_id">
                                    Select payment type
                                </label>
                                <select name="payment_type_id" id="payment_type_id" class="form-control">
                                    <option value="">-Select-</option>
                                    @foreach($paymentTypes as $paymentType)
                                        <option value="{{ $paymentType->id }}">
                                            {{ $paymentType->display_name }} -
                                            {{ $paymentType->amount }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
         <label for="description"><strong>Laboratory payments:</strong></label>
<a href="#" data-toggle="modal" data-target="#myModal">click to select</a>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Laboratory tests</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
       <button type="button" data-dismiss="modal" class="btn btn-primary">Ok</button>
        <button type="button"  class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
                            </div>


                            <button type="submit" class="btn btn-primary">SAVE</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
