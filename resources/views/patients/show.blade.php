@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">
                @include('partials.sidebar')
            </div>
            <div class="col-md-8">
                <div class="row">
                </div>
                <div class="row">
                    <div class="col-sm-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">personal Information for:
                                    {{ $patient->first_name }}
                                    {{ $patient->middle_name }}
                                    {{ $patient->last_name }}
                                </h3>
                            </div>

                            <div class="panel-body">

                                <table class="table table-bordered table-hover">
                                    <tbody>
                                    <tr>
                                        <th>First name</th>
                                        <td>{{ $patient->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Middle name</th>
                                        <td>{{ $patient->middle_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Last name</th>
                                        <td>{{ $patient->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th>Gender</th>
                                        <td>{{ $patient->gender }}</td>
                                    </tr>
                                    <tr>
                                        <th>Date of Birth</th>
                                        <td>{{ $patient->dob }}</td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <td>{{ $patient->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $patient->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>Address</th>
                                        <td>{{ $patient->address }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            @if (Auth::user()->hasRole('receptionist'))
                                <br><br><a href="/patients/{{ $patient->id }}/appointments/create"
                                           class="btn btn-primary">Make
                                    Appointment</a>
                            @endif
                        </div>


                    </div>

                </div>
            </div>
            <div class="col-sm-2">
                @if (Auth::user()->hasRole('doctor'))

                    @include('partials.patient.rightsidebar')

                @endif
                @if (Auth::user()->hasRole('receptionist'))
                    @include('partials/patient/sidebar')
                @endif
            </div>
        </div>
    </div>
@endsection
