<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $gender = ['Female', 'Male'];

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'username' => $faker->unique()->word,
        'gender' => $gender[$faker->numberBetween(0, 1)],
        'dob' => $faker->date(),
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->unique()->phoneNumber,
        'address' => $faker->address,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Appointment::class, function (Faker\Generator $faker) {

    return [
        'patient_id' => function () {
            return factory('App\User')->create()->id;
        },
        'doctor_id' => function () {
            return factory('App\User')->create()->id;
        },
        'description' => $faker->paragraph,
    ];
});

$factory->define(App\PaymentType::class, function (Faker\Generator $faker) {
    $display_name = ucfirst($faker->word);
    $name = str_slug($display_name, '-');

    return [
        'name' => $name,
        'display_name' => $display_name,
        'amount' => $faker->randomNumber(4),
        'description' => $faker->paragraph,
    ];
});

$factory->define(App\Payment::class, function (Faker\Generator $faker) {

    return [
        'user_id' => function () {
            return factory('App\User')->create()->id;
        },
        'payment_type_id' => function () {
            return factory('App\PaymentType')->create()->id;
        },
    ];
});

$factory->define(App\Diagnosis::class, function (Faker\Generator $faker) {

    return [
        'patient_id' => function () {
            return factory('App\User')->create()->id;
        },
        'doctor_id' => function () {
            return factory('App\User')->create()->id;
        },
        'summary' => $faker->paragraph,
        'body' => $faker->paragraph,
    ];
});

$factory->define(App\Department::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->sentence,
        'fee' => $faker->randomNumber(4),
    ];
});

$factory->define(App\drug::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->name,
        'quantity' => $faker->randomNumber(3),
        'price' =>$faker->randomNumber(4),
    ];
});

$factory->define(App\test::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->sentence,
        'price' => $faker->randomNumber(4),
    ];
});
