<?php

use App\PaymentType;
use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentType::truncate();

        PaymentType::create([
            'name' => 'consultation-fee',
            'display_name' => 'Consultation Fee',
            'amount' => 3000,
        ]);
    }
}
