<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::truncate();
        Role::create(['name' => 'admin']);
        Role::create(['name' => 'receptionist']);
        Role::create(['name' => 'laboratory-attendant']);
        Role::create(['name' => 'doctor']);
        Role::create(['name' => 'pharmacist']);
        Role::create(['name' => 'patient']);
    }
}
