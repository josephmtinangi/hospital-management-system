<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public function paymentType()
    {
    	return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }
    
     public function patient()
    {
    	return $this->belongsTo(patient::class);
    }
}
