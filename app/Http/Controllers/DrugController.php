<?php

namespace App\Http\Controllers;

use App\drug;
use Illuminate\Http\Request;

class DrugController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drugs = drug::latest()->get();
        return view('pharmacist.drugs.index', compact('drugs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('pharmacist.drugs.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'item' => 'required|numeric',
            'price' => 'required|numeric'
        ]);
        $drug = new drug();
        $drug->name = $request->input('name');
        $drug->quantity = $request->input('item');
        $drug->price = $request->input('price');
        $drug->save();

        flash('Success!')->success();
        return redirect('medicines');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $drug = drug::findOrFail($id);
        return $drug;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $drug = drug::findOrFail($id);
        return view('pharmacist.drugs.edit', compact('drug'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'item' => 'required|numeric',
            'price' => 'required|numeric'
        ]);
        $drug = drug::findOrFail($id);
        $drug->name = $request->input('name');
        $drug->quantity = $request->input('item');
        $drug->price = $request->input('price');
        $drug->save();

        flash('Updated successfully')->success();

        return redirect('medicines');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param drug $drug
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Request $request, $id)
    {
        $drug = drug::findOrFail($id);

        $drug->delete();

        flash('Deleted successfully')->success();

        return redirect('medicines');
    }

    public function upload(Request $request)
    {
        // load the rows
        $rows = \Excel::load($request->file('user_file'), function ($reader) {

        })->get();

        $count = 0;

        foreach ($rows as $row) {
            $drug = new drug();
            $drug->name = $row->name;
            $drug->quantity = $row->quantity;
            $drug->price = $row->price;
            $drug->save();
        }

        flash($count . ' items uploaded successfully.')->success();

        return redirect('medicines');
    }
}
