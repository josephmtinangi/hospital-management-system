<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Patient;
use App\Prescription;
use App\drug;
use App\Assigndrug;
use Illuminate\Http\Request;

class AssigndrugsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient, $diagnosesId)
    {
        $drugs = drug::all();
        $diagnoses = Diagnosis::whereid($diagnosesId)->get();
        return view('patients.diagnoses.prescriptions.assign_drugs', compact('patient', 'diagnoses', 'drugs', 'diagnosesId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'drug' => 'required',
            'dose' => 'required',
        ]);
        $assigndrug = new Assigndrug();
        $assigndrug->prescription_id = Prescription::latest()->first()->id;
        $assigndrug->medicine_id = $request->input('drug');
        $assigndrug->dose = $request->input('dose');
        $assigndrug->description = $request->input('description');
        $assigndrug->save();
        flash('Success!')->success();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
