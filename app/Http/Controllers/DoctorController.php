<?php

namespace App\Http\Controllers;
use App\Doctor;
use App\User;
use App\Department;
use App\Role;
use App\PaymentType;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    public function index(User $user){
     $doctors =Role::where('name', 'doctor')->first()->users;
    return view('doctors.index',compact('doctors'));
    }
    public function show(Doctor $doctor){

        $deptIds = $doctor->departments()->pluck('department_id');

        $departments = Department::whereNotIn('id', $deptIds)->get();
        
        return view('doctors.show', compact('doctor', 'departments'));   
    }
    public function store(Request $request, Doctor $doctor)
    {    
        $this->validate($request, [
            'department' => 'required',
        ]);

        $department = Department::findOrFail($request->input('department'));
        $doctor->departments()->attach($department);

        return back();
    }
}
