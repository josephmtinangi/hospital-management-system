<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Patient;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $patients = Role::where('name', 'patient')->first()->users()->orderBy('created_at', 'desc')->get();

        $appointments = collect([]);
        if (auth()->user()->hasRole('doctor')) {
            $appointments = auth()->user()->patients()->orderBy('created_at', 'desc')->get();
        }

        return view('patients.index', compact('patients', 'appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => 'required'
        ]);

        $patient = new Patient();
        $patient->first_name = $request->input('first_name');
        $patient->middle_name = $request->input('middle_name');
        $patient->last_name = $request->input('last_name');
        $patient->username = strtolower($request->input('last_name'));
        $patient->gender = $request->input('gender');
        $patient->dob = $request->input('dob');
        $patient->phone = $request->input('phone');
        $patient->email = $request->input('email');
        $patient->address = $request->input('address');
        $patient->password = bcrypt($request->input('last_name'));
        $patient->save();

        $patient->roles()->attach(Role::where('name', 'patient')->first()->id);

        flash('Success!')->success();

        return redirect('patients/' . $patient->id);
    }

    /**
     * Display the specified resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Patient $patient)
    {
        return view('patients.show', compact('patient'));
    }

    public function medicalhistory(Patient $patient)
    {
        return view('patients.appointments.medicalhistory', compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Patient::findOrFail($id);
        return view('patients.edit', compact('patient'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Patient $patient)
    {
        $this->validate($request, [
            'first_name' => 'required|string',
        ]);

        $patient->first_name = $request->input('first_name');
        $patient->middle_name = $request->input('middle_name');
        $patient->last_name = $request->input('last_name');
        $patient->username = strtolower($request->input('last_name'));
        $patient->gender = $request->input('gender');
        $patient->dob = $request->input('dob');
        $patient->phone = $request->input('phone');
        $patient->email = $request->input('email');
        $patient->address = $request->input('address');
        $patient->save();

        flash('Updated successfully.')->success()->important();

        return redirect('patients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
