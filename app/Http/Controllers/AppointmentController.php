<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Doctor;
use App\Patient;
use App\Payment;
use App\PaymentType;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function index(Patient $patient)
    {
        return view('patients.appointments.index', compact('patient'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        $doctors = Role::where('name', 'doctor')->first()->users;
        $paymentTypes = PaymentType::orderBy('name')->get();
        return view('appointments.create', compact('patient', 'doctors', 'paymentTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        $appointment = new Appointment();
        $appointment->patient_id = $patient->id;
        $appointment->doctor_id = $request->input('doctor_id');
        $appointment->description = $request->input('description');
        $appointment->save();

        // make Payment
        $payment = new Payment();
        $payment->patient_id = $patient->id;
        $payment->payment_type_id = $request->input('payment_type_id');
        $payment->save();

        return redirect('patients/' . $patient->id . '/appointments');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient, Appointment $appointment)
    {
        return view('patients.appointments.show', compact('patient', 'appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        //
    }
}
