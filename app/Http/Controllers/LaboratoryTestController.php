<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Test;
use App\Patient;
use Illuminate\Http\Request;

class LaboratoryTestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $laboratoryTests = Test::latest()->get();
        return view('laboratory-tests.index', compact('laboratoryTests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     * @param Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient, Diagnosis $diagnosis)
    {
        //return view('patients.diagnoses.laboratory-tests.create', compact('patient', 'diagnosis'));
        return view('laboratory-tests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric'
        ]);
        /*  $labTest = new LaboratoryTest();
          $labTest->name = $request->input('name');
          $labTest->result = $request->input('result');
          $labTest->patient_id = $patient->id;
          $labTest->doctor_id = auth()->id();
          $labTest->diagnosis_id = $diagnosis->id;
          $labTest->save();*/
        $labTest = new test();
        $labTest->name = $request->input('name');
        $labTest->price = $request->input('price');
        $labTest->save();

        flash('Success')->success();

        return redirect('/laboratory-tests');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param test $test
     * @internal param LaboratoryTest|\App\LaboratoryTest $laboratoryTest
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\LaboratoryTest $laboratoryTest
     */
    public function edit($id)
    {
        $laboratoryTest = Test::findOrFail($id);
        return view('laboratory-tests.edit', compact('laboratoryTest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\LaboratoryTest $laboratoryTest
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required|numeric'
        ]);

        $laboratoryTest = Test::findOrFail($id);
        $laboratoryTest->update($request->only('name', 'price'));

        flash('Updated successfully.')->success();

        return redirect('laboratory-tests');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param \App\LaboratoryTest $laboratoryTest
     */
    public function destroy($id)
    {
        $laboratoryTest = Test::findOrFail($id);
        $laboratoryTest->delete();

        flash('Deleted successfully.')->success();

        return back();
    }

    public function upload(Request $request)
    {
        // load the rows
        $rows = \Excel::load($request->file('user_file'), function ($reader) {

        })->get();

        $count = 0;

        foreach ($rows as $row) {
            $test = new Test();
            $test->name = $row->name;
            $test->price = $row->price;
            $test->save();
            $count++;
        }

        flash($count . ' items uploaded successfully.')->success();

        return redirect('laboratory-tests');
    }
}
