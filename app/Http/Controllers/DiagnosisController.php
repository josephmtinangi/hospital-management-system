<?php

namespace App\Http\Controllers;

use App\Test;
use App\Patient;
use App\Diagnosis;
use App\patient_lab_tests;
use Illuminate\Http\Request;

class DiagnosisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param $patientId
     * @return \Illuminate\Http\Response
     * @internal param Patient $patient
     */
    public function index($patientId)
    {
        $patient = Patient::with(['diagnoses' => function ($query) {
            return $query->orderBy('created_at', 'desc');
        }])->findOrFail($patientId);

        return view('patients.diagnoses.index', compact('patient'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function create(Patient $patient)
    {
        $tests = test::all();
        return view('patients.diagnoses.create', compact('patient', 'tests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient)
    {
        $diagnosis = new Diagnosis();
        $diagnosis->patient_id = $patient->id;
        $diagnosis->doctor_id = auth()->id();
        $diagnosis->summary = $request->input('summary');
        $diagnosis->body = $request->input('body');
        $diagnosis->patient_id = $patient->id;
        $diagnosis->save();

        return redirect('patients/' . $patient->id . '/diagnoses/' . $diagnosis->id . '/laboratory-tests/');
    }

    public function createLabtest(Patient $patient, Diagnosis $diagnosis)
    {
        $labTests = Test::orderBy('name')->get();
        return view('patients.diagnoses.laboratory-tests.create', compact('patient', 'diagnosis', 'labTests'));
    }

    /**
     * @param Request $request
     * @param $patientid
     * @param $diagnosis_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeLabtest(Request $request, $patientId, $diagnosis_id)
    {
        $this->validate($request, [
            'lab_testId' => 'required',
        ]);

        $labtest = new patient_lab_tests();
        $labtest->lab_testId = $request->input('lab_testId');
        $labtest->diagnoses_id = $diagnosis_id;
        $labtest->patient_id = $patientId;
        $labtest->save();

        flash('Success')->success()->important();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param Patient $patient
     * @param Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     * @internal param $diagnosisId
     * @internal param Diagnosis $diagnosis
     */
    public function show(Patient $patient, Diagnosis $diagnosis)
    {
        return view('patients.diagnoses.show', compact('patient', 'diagnosis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function edit(Diagnosis $diagnosis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diagnosis $diagnosis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Diagnosis $diagnosis)
    {
        //
    }
}
