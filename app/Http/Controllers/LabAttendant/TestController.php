<?php

namespace App\Http\Controllers\LabAttendant;

use App\Patient_lab_tests;
use App\Test;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function index()
    {
        if (!auth()->user()->hasRole('laboratory-attendant')) {
            return redirect('login');
        }

        $lab_tests = Patient_lab_tests::with('patient')->latest()->get();

        return view('lab-attendant.tests.index', compact('lab_tests'));
    }

    public function conduct($id)
    {
        $lab_test = Patient_lab_tests::findOrFail($id);
        return view('lab-attendant.tests.conduct', compact('lab_test'));
    }

    public function storeResult(Request $request, $id)
    {
        $this->validate($request, [
            'result' => 'required',
        ]);

        $lab_test = Patient_lab_tests::findOrFail($id);
        $lab_test->result = $request->input('result');
        $lab_test->description = $request->input('description');
        $lab_test->is_conducted = true;
        $lab_test->lab_attendant_id = auth()->id();
        $lab_test->save();

        flash('Success')->success()->important();

        return redirect('lab-tests');
    }
}
