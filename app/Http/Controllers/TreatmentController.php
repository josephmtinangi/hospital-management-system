<?php

namespace App\Http\Controllers;

use App\Diagnosis;
use App\Patient;
use App\Treatment;
use Illuminate\Http\Request;

class TreatmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Patient $patient
     * @return \Illuminate\Http\Response
     */
    public function index(Patient $patient)
    {
        $treatments = $patient->treatments;
        return view('patients.treatments.index', compact('patient', 'treatments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Patient $patient
     * @param Diagnosis $diagnosis
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Patient $patient, Diagnosis $diagnosis)
    {
        $treatment = new Treatment();
        $treatment->patient_id = $patient->id;
        $treatment->diagnosis_id = $diagnosis->id;
        $treatment->doctor_id = auth()->id();
        $treatment->body = $request->input('body');
        $treatment->save();

        flash('Success!')->success();

        return redirect('patients/'.$patient->id.'/treatments');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Treatment $treatment
     * @return \Illuminate\Http\Response
     */
    public function show(Treatment $treatment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Treatment $treatment
     * @return \Illuminate\Http\Response
     */
    public function edit(Treatment $treatment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Treatment $treatment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Treatment $treatment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Treatment $treatment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Treatment $treatment)
    {
        //
    }
}
