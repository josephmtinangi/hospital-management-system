<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnosis extends Model
{
    public function patient_lab_tests()
    {
        return $this->hasMany(patient_lab_tests::class, 'diagnoses_id');
    }

    public function prescriptions()
    {
        return $this->hasMany(Prescription::class);
    }
}
