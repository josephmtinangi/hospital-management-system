<?php

namespace App;
use App\Patient;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
 public function Assigndrugs()
 {
     return $this->hasMany(Assigndrug::class);
 }
 public function patient(){

 	return $this->belongsTo(Patient::class);

 }

 public function drugs()
 {
 	return $this->belongsToMany(drug::class, 'assigndrugs', 'prescription_id', 'medicine_id')->withPivot('dose');
 }
}
