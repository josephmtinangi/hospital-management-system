<?php

Route::get('table', function () {
    return view('table');
});

Route::middleware('auth')->get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::post('laboratory-tests/upload', 'LaboratoryTestController@upload');
    Route::resource('laboratory-tests', 'LaboratoryTestController');

    Route::resource('patients', 'PatientController');
    Route::get('patients/{patient}/medical-history', 'PatientController@medicalhistory');

    Route::resource('users', 'UserController');
    Route::get('users/create', 'UserController@create');
    Route::post('users/new', 'UserController@store');
    Route::post('users/{user}/roles', 'RoleController@store');

    Route::get('doctors/', 'DoctorController@index');
    Route::get('doctors/{doctor}', 'DoctorController@show');
    Route::post('/doctors/{doctor}/departments', 'DoctorController@store');

    Route::resource('appointments', 'AppointmentController');

    Route::get('payment-types', 'PaymentTypeController@index');
    Route::get('payment-types/create', 'PaymentTypeController@create');
    Route::post('payment-types/new', 'PaymentTypeController@store');
    Route::get('payment-types/{id}/edit', 'PaymentTypeController@edit');
    Route::patch('payment-types/{id}', 'PaymentTypeController@update');
    Route::delete('payment-types/{id}', 'PaymentTypeController@destroy');
    Route::resource('payments', 'PaymentController');

    Route::get('patients/{patient}/payments', 'patientpaymentController@index');

    Route::get('patients/{patient}/appointments', 'AppointmentController@index');
    Route::get('patients/{patient}/appointments/create', 'AppointmentController@create');
    Route::get('patients/{patient}/appointments/{appointment}', 'AppointmentController@show');

    Route::get('patients/{patient}/payments/create', 'patientpaymentController@create');
    Route::post('patients/{patient}/appointments', 'AppointmentController@store');

    Route::get('patients/{patient}/diagnoses', 'DiagnosisController@index');

    Route::get('patients/{patient}/diagnoses/{diagnoses}/laboratory-tests', 'DiagnosisController@createLabtest');

    Route::post('patients/{patient}/diagnoses/{diagnoses}/laboratory-tests/store', 'DiagnosisController@storeLabtest');

    Route::get('patients/{patient}/diagnoses/new', 'DiagnosisController@create');
    Route::get('patients/{patient}/diagnoses/{diagnosis}', 'DiagnosisController@show');
    Route::post('patients/{patient}/diagnoses', 'DiagnosisController@store');


    Route::resource('patients.diagnoses.treatment', 'TreatmentController');
    Route::get('patients/{patient}/treatments', 'TreatmentController@index');
    Route::resource('patients.diagnoses.prescriptions', 'PrescriptionController');
    Route::get('patients/{patient}/diagnoses/{diagnose}/prescriptions/create', 'PrescriptionController@create');
    Route::post('patients/{patient}/diagnoses/{diagnosis}/prescriptions/new', 'PrescriptionController@store');
    Route::get('patients/{patient}/diagnoses/{diagnosis}/prescriptions/{prescription}/create/assign-drugs', 'AssigndrugsController@create');
    Route::post('patients/{$x}/diagnoses/{$y}/prescriptions/{$z}/assign-drugs', 'AssigndrugsController@store');

    Route::post('medicines/upload', 'DrugController@upload');
    Route::get('medicines', 'DrugController@index');
    Route::get('medicines/{medicine}/edit', 'DrugController@edit');
    Route::patch('medicines/{medicine}', 'DrugController@update');
    Route::get('medicines/create', 'DrugController@create');
    Route::post('medicines/new', 'DrugController@store');
    Route::delete('medicines/{medicine}', 'DrugController@destroy');


    Route::post('departments/upload', 'DrugController@upload');
    Route::get('departments', 'DepartmentController@index');
    Route::get('departments/{department}/edit', 'DepartmentController@edit');
    Route::patch('departments/{departments}', 'DepartmentController@update');
    Route::get('departments/create', 'DepartmentController@create');
    Route::post('departments/new', 'DepartmentController@store');
    Route::delete('departments/{departments}', 'DepartmentController@destroy');

    Route::get('pharmace/prescriptions', 'pharmacist\PrescriptionController@index');

    Route::get('/download/{filename}', 'DownloadController@index');

    Route::get('lab-tests', 'LabAttendant\TestController@index');
    Route::get('lab-tests/{id}/conduct', 'LabAttendant\TestController@conduct');
    Route::post('lab-tests/{id}/conduct', 'LabAttendant\TestController@storeResult');
});